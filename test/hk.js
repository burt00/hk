(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){


let HaikangPlayClass =  function(config){
    this.native = null;
    this.config = config;
    this.visibile = true;
}

HaikangPlayClass.prototype._request = require("./_request.js");
HaikangPlayClass.prototype.adaptive =  require("./adaptive.js");
HaikangPlayClass.prototype.initView = require("./initView.js");
HaikangPlayClass.prototype.play =  require("./play.js");
HaikangPlayClass.prototype.stop =  require("./stop.js");
HaikangPlayClass.prototype.playBack =  require("./playBack.js");
HaikangPlayClass.prototype.show =  require("./show.js");
HaikangPlayClass.prototype.hide =  require("./hide.js");
HaikangPlayClass.prototype.destroy =  require("./destroy.js");
HaikangPlayClass.prototype.setAuthInfo =  require("./setAuthInfo.js");
HaikangPlayClass.prototype.setLayout =  require("./setLayout.js");
HaikangPlayClass.prototype.getLayout =  require("./getLayout.js");


module.exports = HaikangPlayClass;
},{"./_request.js":2,"./adaptive.js":3,"./destroy.js":4,"./getLayout.js":5,"./hide.js":6,"./initView.js":7,"./play.js":9,"./playBack.js":10,"./setAuthInfo.js":11,"./setLayout.js":12,"./show.js":13,"./stop.js":14}],2:[function(require,module,exports){


  module.exports  = async function(options){
    let {native} = this;
    if (options.argument) {
      options.argument = JSON.stringify(options.argument);
    }

    let result = await native.JS_RequestInterface(options)

    if(result.responseMsg.code==0){
      try{
        let data = JSON.parse(result.responseMsg.data);
        return data;
      }catch(err){
        return "";
      }
    }else{
      console.error(result.responseMsg.msg);
      return null;
    }
    
  }
},{}],3:[function(require,module,exports){
module.exports = function () {
  let { native, config } = this;
  console.log("自适应");
  const el = document.getElementById(config.id);
  let w = el.offsetWidth;
  let h = el.offsetHeight;
  native.JS_Resize(w, h);

  //获取web页面的尺寸
  var iWidth = window.outerWidth;
  var iHeight = window.outerHeight;
  //获取播放窗口div元素的左边界、右边界距离web页面左边界的长度、上边界、下边界距离web页面上边界的长度
  var oDivRect = document.getElementById(config.id).getBoundingClientRect();
  //Math.round 为四舍五入    Math.abs 为取绝对值
  var iCoverLeft = oDivRect.left < 0 ? Math.abs(oDivRect.left) : 0;
  var iCoverTop = oDivRect.top < 0 ? Math.abs(oDivRect.top) : 0;
  var iCoverRight =
    oDivRect.right - iWidth > 0 ? Math.round(oDivRect.right - iWidth) : 0;
  var iCoverBottom =
    oDivRect.bottom - iHeight > 0 ? Math.round(oDivRect.bottom - iHeight) : 0;

  iCoverLeft = iCoverLeft > w ? w : iCoverLeft;
  iCoverTop = iCoverTop > h ? h : iCoverTop;
  iCoverRight = iCoverRight > w ? w : iCoverRight;
  iCoverBottom = iCoverBottom > h ? h : iCoverBottom;

  native.JS_RepairPartWindow(0, 0, w + 1, h); // 多1个像素点防止还原后边界缺失一个像素条
  //抠除左边界
  if (iCoverLeft != 0) {
    native.JS_CuttingPartWindow(0, 0, iCoverLeft, h + 1);
  }
  //抠除上边界
  if (iCoverTop != 0) {
    native.JS_CuttingPartWindow(0, 0, w + 1, iCoverTop); // 多剪掉一个像素条，防止出现剪掉一部分窗口后出现一个像素条
  }
  //抠除右边界
  if (iCoverRight != 0) {
    native.JS_CuttingPartWindow(
      w + 1 - iCoverRight,
      0,
      iCoverRight,
      h + 1
    );
  }
  //抠除下边界
  if (iCoverBottom != 0) {
    native.JS_CuttingPartWindow(
      0,
      h + 1 - iCoverBottom,
      w + 1,
      iCoverBottom
    );
  }

};

},{}],4:[function(require,module,exports){
module.exports = function () {
  let { native } = this;

  //反初始化
  this._request({
    funcName: "uninit",
  });
  
  //销毁
  native.JS_DestroyWnd();
};

},{}],5:[function(require,module,exports){
module.exports = function () {
  return this._request({
    funcName: "getLayout",
  });
};

},{}],6:[function(require,module,exports){
module.exports = function(){
    let {native,visibile,config} = this;
    if(visibile==true){
        native.JS_HideWnd();
        document.getElementById(config.id).innerHTML = "";
        this.visibile = false;
    }
}

},{}],7:[function(require,module,exports){
module.exports = function () {
  let { config } = this;
  let data = {
    funcName: "init",
    argument: {
      appkey: "",
      ip: "",
      port: 0,
      secret: "",
      enableHTTPS: 1,
      language: "zh_CN",
      layout: "1x1",
      playMode: 0,
      reconnectDuration: 5,
      reconnectTimes: 5,
      showSmart: 0,
      showToolbar: 1,
      toolBarButtonIDs:
        "2048,2049,2050,2304,2306,2305,2307,2308,2309,4096,4608,4097,4099,4098,4609,4100",
      snapDir: "D:/snap",
      videoDir: "D:/video",
    },
  };
  
  if (Object.prototype.toString.call(config.info) == "[object Object]") {
    for (let key in config.info) {
      data.argument[key] = config.info[key];
    }
  }
  return this._request(data);
};

},{}],8:[function(require,module,exports){
(function (global){(function (){
let HaikangPlayClass = require("./HaikangPlayClass.js");
let initCount = 0;
let Haikang = function (config) {
  let instance = new HaikangPlayClass(config);
  return new Promise((resolve, reject) => {
    //初始化
    let init = function () {
      return new WebControl({
        szPluginContainer: config.id,
        iServicePortStart: 15900,
        iServicePortEnd: 15909,
        szClassId: "23BF3B0A-2C56-4D97-9C03-0CB103AA8F11", // 用于IE10使用ActiveX的clsid
        cbConnectSuccess: async function () {
          try {
            let { native } = instance;

            if (
              Object.prototype.toString.call(config.callback) ==
              "[object Function]"
            ) {
              //设置回调消息
              native.JS_SetWindowControlCallback({
                cbIntegrationCallBack: config.callback,
              });
              // console.log("设置回调消息");
            }

            //启动插件服务接口
            await native.JS_StartService("window", {
              dllPath: "./VideoPluginConnect.dll",
            });

            //创建插件窗口接口
            await native.JS_CreateWnd(config.id, 0, 0);
            // console.log("创建插件窗口接口");

            //申请公钥
            // await self.setRSA();

            //初始化显示
            instance.initView();
            // console.log("初始化显示");

            //窗口适应
            instance.adaptive();
          } catch (err) {
            console.error(err);
          }

          resolve(instance);
        },
        cbConnectError: function () {
          console.log("启动失败正在尝试重新启动");
          WebControl.JS_WakeUp("VideoWebPlugin://");
          initCount++;
          if (self.initCount < 3) {
            setTimeout(function () {
              init();
            }, 3000);
          } else {
            reject();
            alert("插件启动失败！");
          }
        },
        cbConnectClose: function (bNormalClose) {
          // 异常断开：bNormalClose = false
          // JS_Disconnect正常断开：bNormalClose = true
          console.log("cbConnectClose", bNormalClose);
        },
      });
    };
    instance.native = init();
  }).catch((err) => {
    console.error(err);
  });
};

global.hk = Haikang;

}).call(this)}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./HaikangPlayClass.js":1}],9:[function(require,module,exports){
module.exports = function (data) {
  let type = Object.prototype.toString.call(data);
  let list = [];

  //判断如果是字符串
  if (type == "[object String]") {
    list.push({
      cameraIndexCode: data,
      wndId: 1, //如果传入单个默认使用第一个窗口
    });
  } else if (type == "[object Array]") {
    for (let key in data) {
      let obj = data[key];
      if ((Object.prototype, toString.call(obj) == "[object Object]")) {
        list.push(obj);
      } else {
        console.error("An array can only contain objects:", data);
        return;
      }
    }
  } else {
    console.error("Error in passing parameter type:", data);
    return;
  }

  return this._request({
    funcName: "startMultiPreviewByCameraIndexCode",
    argument: {
      list: list,
    },
  })
  

};

},{}],10:[function(require,module,exports){
module.exports = function (data) {
  let type = Object.prototype.toString.call(data);
  let list = [];

  //判断是否为对象
  if (type == "[object Array]") {
    for (let key in data) {
      let obj = data[key];
      if ((Object.prototype, toString.call(obj) == "[object Object]")) {
        if( obj.wndId == undefined || obj.wndId == null){
          obj.wndId = 1;
        }
        list.push(obj);
      
        
      } else {
        console.error("An array can only contain objects:", data);
        return;
      }
    }
  } else {
    console.error("Error in passing parameter type:", data);
    return;
  }

  return this._request({
    funcName: "startMultiPlaybackByCameraIndexCode",
    argument: {
      list: list,
    },
  });
};

},{}],11:[function(require,module,exports){
module.exports = function (data) {
  let type = Object.prototype.toString.call(data);
  let list = [];

  //判断是否为对象
  if (type == "[object Array]") {
    for (let key in data) {
      let obj = data[key];
      if ((Object.prototype, toString.call(obj) == "[object Object]")) {
        list.push(obj);
      } else {
        console.error("An array can only contain objects:", data);
        return;
      }
    }
  } else {
    console.error("Error in passing parameter type:", data);
    return;
  }

  return this._request({
    funcName: "setAuthInfo",
    argument: {
      list: list,
    },
  });
};

},{}],12:[function(require,module,exports){
module.exports = async function (layout) {
  return this._request({
    funcName: "setLayout",
    argument: {
      layout:layout
    },
  });
};

},{}],13:[function(require,module,exports){
module.exports = function(){
    let {native,visibile} = this;
    if(visibile==false){
        native.JS_ShowWnd();
        this.visibile = true;
    }
}
},{}],14:[function(require,module,exports){
module.exports = function (data) {
    let type = Object.prototype.toString.call(data);
    let list = [];
  
    if (type == "[object Array]") {
      for (let key in data) {
        let wndId = data[key];
        list.push({wndId});
      }
    } else {
      console.error("Error in passing parameter type:", data);
      return;
    }
  
    return this._request({
      funcName: "stopMultiPlay",
      argument: {
        list: list,
      },
    })
    
  
  };
  
},{}]},{},[8]);
