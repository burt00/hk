module.exports = function () {
  let { native, config } = this;
  console.log("自适应");
  const el = document.getElementById(config.id);
  let w = el.offsetWidth;
  let h = el.offsetHeight;
  native.JS_Resize(w, h);

  //获取web页面的尺寸
  var iWidth = window.outerWidth;
  var iHeight = window.outerHeight;
  //获取播放窗口div元素的左边界、右边界距离web页面左边界的长度、上边界、下边界距离web页面上边界的长度
  var oDivRect = document.getElementById(config.id).getBoundingClientRect();
  //Math.round 为四舍五入    Math.abs 为取绝对值
  var iCoverLeft = oDivRect.left < 0 ? Math.abs(oDivRect.left) : 0;
  var iCoverTop = oDivRect.top < 0 ? Math.abs(oDivRect.top) : 0;
  var iCoverRight =
    oDivRect.right - iWidth > 0 ? Math.round(oDivRect.right - iWidth) : 0;
  var iCoverBottom =
    oDivRect.bottom - iHeight > 0 ? Math.round(oDivRect.bottom - iHeight) : 0;

  iCoverLeft = iCoverLeft > w ? w : iCoverLeft;
  iCoverTop = iCoverTop > h ? h : iCoverTop;
  iCoverRight = iCoverRight > w ? w : iCoverRight;
  iCoverBottom = iCoverBottom > h ? h : iCoverBottom;

  native.JS_RepairPartWindow(0, 0, w + 1, h); // 多1个像素点防止还原后边界缺失一个像素条
  //抠除左边界
  if (iCoverLeft != 0) {
    native.JS_CuttingPartWindow(0, 0, iCoverLeft, h + 1);
  }
  //抠除上边界
  if (iCoverTop != 0) {
    native.JS_CuttingPartWindow(0, 0, w + 1, iCoverTop); // 多剪掉一个像素条，防止出现剪掉一部分窗口后出现一个像素条
  }
  //抠除右边界
  if (iCoverRight != 0) {
    native.JS_CuttingPartWindow(
      w + 1 - iCoverRight,
      0,
      iCoverRight,
      h + 1
    );
  }
  //抠除下边界
  if (iCoverBottom != 0) {
    native.JS_CuttingPartWindow(
      0,
      h + 1 - iCoverBottom,
      w + 1,
      iCoverBottom
    );
  }

};
