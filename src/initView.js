module.exports = function () {
  let { config } = this;
  let data = {
    funcName: "init",
    argument: {
      appkey: "",
      ip: "",
      port: 0,
      secret: "",
      enableHTTPS: 1,
      language: "zh_CN",
      layout: "1x1",
      playMode: 0,
      reconnectDuration: 5,
      reconnectTimes: 5,
      showSmart: 0,
      showToolbar: 1,
      toolBarButtonIDs:
        "2048,2049,2050,2304,2306,2305,2307,2308,2309,4096,4608,4097,4099,4098,4609,4100",
      snapDir: "D:/snap",
      videoDir: "D:/video",
    },
  };
  
  if (Object.prototype.toString.call(config.info) == "[object Object]") {
    for (let key in config.info) {
      data.argument[key] = config.info[key];
    }
  }
  return this._request(data);
};
