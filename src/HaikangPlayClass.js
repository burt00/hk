

let HaikangPlayClass =  function(config){
    this.native = null;
    this.config = config;
    this.visibile = true;
}

HaikangPlayClass.prototype._request = require("./_request.js");
HaikangPlayClass.prototype.adaptive =  require("./adaptive.js");
HaikangPlayClass.prototype.initView = require("./initView.js");
HaikangPlayClass.prototype.play =  require("./play.js");
HaikangPlayClass.prototype.stop =  require("./stop.js");
HaikangPlayClass.prototype.playBack =  require("./playBack.js");
HaikangPlayClass.prototype.show =  require("./show.js");
HaikangPlayClass.prototype.hide =  require("./hide.js");
HaikangPlayClass.prototype.destroy =  require("./destroy.js");
HaikangPlayClass.prototype.setAuthInfo =  require("./setAuthInfo.js");
HaikangPlayClass.prototype.setLayout =  require("./setLayout.js");
HaikangPlayClass.prototype.getLayout =  require("./getLayout.js");


module.exports = HaikangPlayClass;