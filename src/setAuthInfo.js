module.exports = function (data) {
  let type = Object.prototype.toString.call(data);
  let list = [];

  //判断是否为对象
  if (type == "[object Array]") {
    for (let key in data) {
      let obj = data[key];
      if ((Object.prototype, toString.call(obj) == "[object Object]")) {
        list.push(obj);
      } else {
        console.error("An array can only contain objects:", data);
        return;
      }
    }
  } else {
    console.error("Error in passing parameter type:", data);
    return;
  }

  return this._request({
    funcName: "setAuthInfo",
    argument: {
      list: list,
    },
  });
};
