module.exports = function (data) {
    let type = Object.prototype.toString.call(data);
    let list = [];
  
    if (type == "[object Array]") {
      for (let key in data) {
        let wndId = data[key];
        list.push({wndId});
      }
    } else {
      console.error("Error in passing parameter type:", data);
      return;
    }
  
    return this._request({
      funcName: "stopMultiPlay",
      argument: {
        list: list,
      },
    })
    
  
  };
  