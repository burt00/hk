module.exports = function (data) {
  let type = Object.prototype.toString.call(data);
  let list = [];

  //判断如果是字符串
  if (type == "[object String]") {
    list.push({
      cameraIndexCode: data,
      wndId: 1, //如果传入单个默认使用第一个窗口
    });
  } else if (type == "[object Array]") {
    for (let key in data) {
      let obj = data[key];
      if ((Object.prototype, toString.call(obj) == "[object Object]")) {
        list.push(obj);
      } else {
        console.error("An array can only contain objects:", data);
        return;
      }
    }
  } else {
    console.error("Error in passing parameter type:", data);
    return;
  }

  return this._request({
    funcName: "startMultiPreviewByCameraIndexCode",
    argument: {
      list: list,
    },
  })
  

};
