module.exports = function () {
  let { native } = this;

  //反初始化
  this._request({
    funcName: "uninit",
  });
  
  //销毁
  native.JS_DestroyWnd();
};
