

  module.exports  = async function(options){
    let {native} = this;
    if (options.argument) {
      options.argument = JSON.stringify(options.argument);
    }

    let result = await native.JS_RequestInterface(options)

    if(result.responseMsg.code==0){
      try{
        let data = JSON.parse(result.responseMsg.data);
        return data;
      }catch(err){
        return "";
      }
    }else{
      console.error(result.responseMsg.msg);
      return null;
    }
    
  }