module.exports = async function (layout) {
  return this._request({
    funcName: "setLayout",
    argument: {
      layout:layout
    },
  });
};
